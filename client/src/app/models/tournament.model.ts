// import { Script } from 'vm';

export class Tournament {
    id: number;
    name: string;
    region: string;
    date: Date;
    format: string;
    type: string;

}