import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'

import { ReactiveFormsModule } from '@angular/forms';

import { TournamentsService } from './tournaments.service'

import { HomeComponent } from './home/home.component';
import { TournamentsBrowseComponent } from './tournaments-browse/tournaments-browse.component';
import { TournamentsOrganizeComponent } from './tournaments-organize/tournaments-organize.component';
import { TournamentFormUpdateComponent } from './tournament-form-update/tournament-form-update.component';
import { PlayersComponent } from './players/players.component';


const routes: Routes = [

  { path: '', component: HomeComponent },
  { path: 'browse', component: TournamentsBrowseComponent },
  { path: 'organize', component: TournamentsOrganizeComponent },
  { path: 'browse/tournament/:id/players', component: PlayersComponent }  
  
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TournamentsBrowseComponent,
    TournamentsOrganizeComponent,
    TournamentFormUpdateComponent,
    PlayersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
  ],
  providers:  [ TournamentsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
