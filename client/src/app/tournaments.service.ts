import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Router } from '@angular/router'
import { Tournament } from './models/tournament.model'

@Injectable()
export class TournamentsService {

  constructor(private http: HttpClient, private router: Router) {

  }

  findAllAvailableTournaments(): Observable<Tournament[]> {
    return this.http.get(`/tournaments`)
      .pipe(
        map((result: Tournament[]) => {
        return result;
      }));
  }

  addNewTournamentToDatabase(tournament: Tournament) {
    return this.http.post("/tournaments", tournament);
  }

  deleteTournament(id: number){
    return this.http.delete(`/tournaments/${id}`);
  }

  updateTournament(id: number, tournament: Tournament){
    return this.http.put(`/tournaments/${id}`, tournament);
  }

  getPlayers(tid: number) :Observable<any> {
    return this.http.get(`/tournaments/${tid}/players`)
      .pipe(
        map((result) => {
        return JSON.stringify(result);
      }));
  }

}