import { Component } from '@angular/core';
import { TournamentsService } from './tournaments.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public tournamentsService: TournamentsService){
    
  }
  
  title = 'client';
}
