import { Component, OnInit } from '@angular/core';
import { TournamentsService } from '../tournaments.service';
import { Observable } from 'rxjs';
import { Tournament } from '../models/tournament.model';
import { TournamentForm } from './models/tournament.form';

@Component({
    selector: 'app-browse-ceva',
    templateUrl: './tournaments-organize.component.html',
    styleUrls: ['./tournaments-organize.component.css']
  })
export class TournamentsOrganizeComponent{
    private availableTournaments: any;
    private tournaments: Tournament[];

    addTournamentForm: TournamentForm;

    private showForm: boolean = false;

    constructor(public tournamentsService: TournamentsService) {
        this.addTournamentForm = new TournamentForm();
     }

  ngOnInit() {
      
  }

  showCreateTournamentForm() {
      this.showForm = true;
      console.log('showForm ', this.showForm);
  }


  addNewTournamentToDatabase() {
      if(this.addTournamentForm){
          const formCopy = JSON.parse(JSON.stringify(this.addTournamentForm.value));
          console.log('formCopy ', formCopy);
          
          this.tournamentsService.addNewTournamentToDatabase(this.addTournamentForm.value)
          .subscribe(
              () => {
                console.log('it works');
                this.addTournamentForm.refreshForm();
              }
          )
      }
  }


}