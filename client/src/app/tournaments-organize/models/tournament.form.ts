import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Tournament } from '../../models/tournament.model';

export class TournamentForm extends FormGroup {
    constructor(formdata?){
        super({
            name: new FormControl((formdata && formdata.name) || ''),
            region: new FormControl((formdata && formdata.region) || ''),
            date: new FormControl((formdata && formdata.date) || ''),
            format: new FormControl((formdata && formdata.format) || ''),
            type: new FormControl((formdata && formdata.type) || '')
        });
    }

    public refreshForm() {
        this.get('name').setValue('');
        this.get('region').setValue('');
        this.get('date').setValue('');
        this.get('format').setValue('');
        this.get('type').setValue('');
    }

    public setForm(tournament: Tournament){
        this.get('name').setValue(tournament.name);
        this.get('region').setValue(tournament.region);
        this.get('date').setValue(tournament.date);
        this.get('format').setValue(tournament.format);
        this.get('type').setValue(tournament.type);
    }
}

// export interface Tournament {
//     name: string;
//     region: string;
//     date: Date;
//     format: string;
//     type: string;
// }