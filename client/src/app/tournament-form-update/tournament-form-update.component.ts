import { Component, OnInit, Input, Output, SimpleChanges } from '@angular/core';
import { TournamentForm } from '../tournaments-organize/models/tournament.form';
import { TournamentsService } from './../tournaments.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tournament-form-update',
  templateUrl: './tournament-form-update.component.html',
  styleUrls: ['./tournament-form-update.component.css']
})
export class TournamentFormUpdateComponent implements OnInit {

  @Input() selectedTournament;
  @Output() showFormChange = new EventEmitter();

  @Output() tournamentWasUpdated = new EventEmitter();

  addTournamentForm: TournamentForm;

  
  constructor(public tournamentsService: TournamentsService) {
    this.addTournamentForm = new TournamentForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.selectedTournament && !changes.selectedTournament.firstChange){
      this.addTournamentForm.setForm(this.selectedTournament);
    }

  }

  ngOnInit() {
    console.log('this.selectedTournament inside FormUpdatecOMPONENT ', this.selectedTournament);
    this.addTournamentForm.refreshForm();
    if(this.selectedTournament){
      this.addTournamentForm.setForm(this.selectedTournament);
    }
  }

  updateTournament() {
    if(this.addTournamentForm) {
      this.tournamentsService.updateTournament(this.selectedTournament.id, this.addTournamentForm.value).subscribe(
        () => {
          this.tournamentWasUpdated.emit('updated');
      })
    }
  }

  onCancel(){
    this.showFormChange.emit('false');
  }


}
