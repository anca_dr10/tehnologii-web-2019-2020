import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentFormUpdateComponent } from './tournament-form-update.component';

describe('TournamentFormUpdateComponent', () => {
  let component: TournamentFormUpdateComponent;
  let fixture: ComponentFixture<TournamentFormUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentFormUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentFormUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
