import { Component, OnInit } from '@angular/core';
import { TournamentsService } from '../tournaments.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {
  private players: any;
  private id: number;

  constructor(public tournamentsService: TournamentsService,
              public activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];

      this.getPlayers();
    });
  }


  getPlayers() {
    this.tournamentsService.getPlayers(this.id).subscribe(result => {
      this.players = result;
      console.log(this.players);
    });
  }

}
