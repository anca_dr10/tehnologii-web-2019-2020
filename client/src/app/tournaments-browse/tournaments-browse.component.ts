import { Component, OnInit } from '@angular/core';
import { TournamentsService } from '../tournaments.service';
import { Observable } from 'rxjs';
import { Tournament } from '../models/tournament.model';

@Component({
  selector: 'app-browse-ceva',
  templateUrl: './tournaments-browse.component.html',
  styleUrls: ['./tournaments-browse.component.css']
})
export class TournamentsBrowseComponent {
  private availableTournaments: any;
  private tournaments: Tournament[];

  private showForm: boolean = false;
  private showPlayers: boolean = false;

  private selectedTournament: Tournament = new Tournament();

  private tournament: Tournament = new Tournament();

  private players: any;

  constructor(public tournamentsService: TournamentsService) {

  }

  ngOnInit() {
    this.getAvailableTournaments();
  }

  getAvailableTournaments() {
    this.tournamentsService.findAllAvailableTournaments().subscribe(data => {
      this.tournaments = data;
    });
  }


  onDeleteTournament(tournament) {
    this.tournamentsService.deleteTournament(tournament.id).subscribe(
      () => {
        // if successfully deleted then update the list
        this.getAvailableTournaments();
      });
  }

  onUpdateTournament(tournament){
    this.selectedTournament = tournament;
    this.showForm = true;
  }

  onShowFormVariableChangeEvent(console){
    this.showForm = false;
  }

  onTournamentWasUpdatedChange(event) {
    if( event == 'updated') {
      this.getAvailableTournaments();
    }
  }

  onPlayerDetails(tournament){
    console.log('do something');

    this.tournamentsService.getPlayers(tournament.id).subscribe(result => {
      this.players = result;
      this.showPlayers = true;
      console.log(result);
    });
  }

  



}