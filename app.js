const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('tournaments_db', 'root', 'makeawish', {
    dialect: 'mysql',
    operatorsAliases: Sequelize.Op,
    define: {
        timestamps: false  //nu mai pune 'created at' si 'updated at'
    }
})

sequelize.authenticate()
    .then(() => console.log('we are connected'))
    .catch((error) => console.log(error))

let User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [3, 20]
        }
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [3, 30],
            isAlpha: true
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [4, 15]
        }
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true
        }
    }
})

const Tournament = sequelize.define('tournament', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [3 - 70]
        }
    },
    region: {
        type: Sequelize.STRING,
        allowNull: false
    },

    date: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },

    format: {
        type: Sequelize.STRING,
        allowNull: true
    },

    type: {
        type: Sequelize.STRING,
        allowNull: true
    }
});

Tournament.hasMany(User)
User.belongsTo(Tournament)         

const app = express();
app.use(bodyParser.json());

sequelize.sync()
    .then(() => console.log('created'))
    .catch((error) => console.log(error))

var availableTournaments = [
    { name: 'MGA 5v5 League of Legends #1', region: 'North America', date: '2017-06-15', format: '5v5', type: 'Tournament Draft' },
    { name: 'Call of Duty - Sniper Elite Squad', region: 'International', date: '2017-06-15', format: 'Squads', type: '' },
    { name: 'NG C-ops 5vs5 EU (13 Rounds) #4', region: 'Europe', date: '2017-06-15', format: '5v5', type: '' }
]

Tournament.bulkCreate(availableTournaments)
.then(function() {
    return Tournament.findAll()
  })
  .then(function(tournaments){
      return tournaments;
  });

  app.get('/tournaments', async (req, res) => {
    try {
        let tournaments = await Tournament.findAll()
        res.status(200).json(tournaments)
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'server error' })
    }
})


// sequelize.sync()
//     .then(() => console.log('created'))
//     .catch((error) => console.log(error))

app.post('/tournaments', async (req, res) => {
    try {
        //query parameter
        if (req.query.bulk && req.query.bulk == 'on') {  
            await Tournament.bulkCreate(req.body)
            res.status(201).json({ message: 'created' })
        }
        else {
            await Tournament.create(req.body)
            res.status(201).json({ message: 'created' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'server error' })
    }
});

app.delete('/tournaments/:id', async (req, res) => {
	try {
		let tournament = await Tournament.findByPk(req.params.id);
		if (tournament) {
			await tournament.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e) {
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.put('/tournaments/:id', async (req, res) => {
	try {
		let tournament = await Tournament.findByPk(req.params.id)
		if (tournament) {
			await tournament.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else {
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

app.post('/tournaments/:tid/players', async (req, res, next) => {
    try {
        let tournament = await Tournament.findByPk(req.params.tid)
        if (tournament) {
            let user = req.body
            user.tournamentId = tournament.id
            await User.create(user)
            res.status(201).json({ message: 'created' })
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (e) {
        next(e);
    }
});

app.get('/tournaments/:tid/players', async(req, res, next) => {
    try {
        let tournament = await Tournament.findByPk(req.params.tid, {
            include: [User]   //jonction
        });
        if (tournament) {
            res.status(200).json(tournament.users);

        } else {
            res.status(404).json({ message: "book not found" })
        }
    }
    catch (err) {
        next(err);
    }
});


app.get('/tournaments/:tid/players/:pid', async (req, res) => {
    try {
        let tournament = await Tournament.findByPk(req.params.tid)
        if (tournament) {

            let players = await tournament.getUsers({
                where: {
                    id: req.params.pid
                }
            });;
            let player = players.shift();
           
            if (player){
                res.status(200).json(player);
            } else {
                res.status(200).json({ message: 'player not found '});
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'server error' })
    }
});


app.put('/tournaments/:tid/players/:pid', async(req, res, next) => {
    try {
        let tournament = await Tournament.findByPk(req.params.tid);
        if (tournament) {
    
            let players = await tournament.getUsers({
                where: {
                    id: req.params.pid
                }
            });
            let player = players.shift()  
            if (player) {
                await player.update(req.body, {
                    fields: ['username', 'name', 'password', 'email']
                });
                res.status(202).json({ message: 'player updated' })
            } else {
                res.status(404).json({ message: "player not found" })
            }
        } else {
            res.status(404).json({ message: "tournament not found" })
        }
    }
    catch (err) {
        next(err);
    }

});

app.delete('/tournaments/:tid/players/:pid', async(req, res, next) => {

    try {
        let tournament = await Tournament.findByPk(req.params.tid);
        if (tournament) {
    
            let players = await tournament.getUsers({
                where: {
                    id: req.params.pid
                }
            });
            let player = players.shift()  
            if (player) {
                await player.destroy();
                res.status(202).json({ message: "player was deleted "})
    
            } else {
                res.status(404).json({ message: "player not found" })
            }
        } else {
            res.status(404).json({ message: "tournament not found" })
        }
    }
    catch (err) {
        next(err);
    }
});

app.listen(8080);


